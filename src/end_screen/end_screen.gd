extends Control

func open():
	visible = true
	get_tree().paused = true

func _on_Quit_pressed():
	get_tree().paused = false
	get_tree().change_scene("res://src/main_menu/main_menu.tscn")