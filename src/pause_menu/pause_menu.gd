extends Control

func _process(delta):
	if Input.is_action_just_pressed("pause"):
		if visible:
			close()
		else:
			open()
	
	
func open():
	visible = true
	get_tree().paused = true
	
	
func close():
	visible = false
	get_tree().paused = false


func _on_Quit_pressed():
	get_tree().paused = false
	get_tree().change_scene("res://src/main_menu/main_menu.tscn")


func _on_Close_pressed():
	close()
