extends Node

signal open_door

var lit_sconces: Dictionary = {
	1: false,
	2: false,
};

func sconce_lit(sconce: int):
	lit_sconces[sconce] = true

	# See if all sconces are now lit
	for s in lit_sconces.values():
		if s == false:
			return
	# All sconces should be lit
	emit_signal("open_door")
	