extends Control


func _on_button_down(action: String):
	Input.action_press(action)


func _on_button_up(action: String):
	Input.action_release(action)
