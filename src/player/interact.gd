extends "res://src/state.gd"

export(NodePath) var animation_tree_path
onready var animation_tree: AnimationTree = get_node(animation_tree_path)
onready var animation_sm: AnimationNodeStateMachinePlayback = animation_tree.get("parameters/playback")

export(NodePath) var grab_area_path
onready var grab_area: Area = get_node(grab_area_path)

export(NodePath) var grabbed_item_path
onready var grabbed_item: Spatial = get_node(grabbed_item_path)

func enter():
	# TODO: Make sure it just auto transitions whenever we throw away animation tree statemachine which is sucky now.
	animation_sm.travel("interact")
	
	var bodies = grab_area.get_overlapping_bodies()
	
	# Use held item
	if grabbed_item.is_holding_something():
		var item = grabbed_item.get_child(0)
		if item.has_method("use"):
			item.use(bodies)
		return

	for body in bodies:
		if body.has_method("activate"):
			body.activate()

func update(delta):
	# If you emit this in the same frame as enter, then the janky animation won't play
	emit_signal("finished", "idle")
	