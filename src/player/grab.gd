extends "res://src/state.gd"

export(NodePath) var animation_tree_path
onready var animation_tree: AnimationTree = get_node(animation_tree_path)
onready var animation_sm = animation_tree.get("parameters/playback")

export(NodePath) var grab_area_path
onready var grab_area: Area = get_node(grab_area_path)

export(NodePath) var grabbed_item_path
onready var grabbed_item: Spatial = get_node(grabbed_item_path)

func enter():
	animation_sm.travel("grab")
	
	if grabbed_item.is_holding_something():
		emit_signal("finished", "drop")
		return
		
	var bodies = grab_area.get_overlapping_bodies()
	for body in bodies:
		if body.is_in_group("grabbable"):
			body.get_parent().remove_child(body)
			grabbed_item.add_child(body)
			body.global_transform.origin = grabbed_item.global_transform.origin
			body.grab()
			break
	
	emit_signal("finished", "previous")
	
