extends "res://src/state.gd"

export(NodePath) var animation_tree_path
onready var animation_tree: AnimationTree = get_node(animation_tree_path)
onready var animation_sm = animation_tree.get("parameters/playback")

func enter():
	animation_sm.travel("idle")
	
	
# warning-ignore:unused_argument
func update(delta):
	for action in ["move_forward", "move_back", "move_right", "move_left"]:
		if Input.is_action_pressed(action):
			emit_signal("finished", "walking")
			
	if Input.is_action_just_pressed("grab_and_drop"):
		emit_signal("finished", "grab")
		
	if Input.is_action_just_pressed("interact"):
		emit_signal("finished", "interact")

