extends "res://src/state.gd"

export(NodePath) var grab_area_path
onready var grab_area: Area = get_node(grab_area_path)

export(NodePath) var grabbed_item_path
onready var grabbed_item: Spatial = get_node(grabbed_item_path)

func enter():
	var item: Spatial = grabbed_item.get_child(0)
	grabbed_item.remove_child(item)
	get_tree().get_root().add_child(item)
	item.global_transform.origin = grab_area.global_transform.origin
	item.drop()
	
	# If grab was last state, we'd get stuck in a loop. Need to figure out how to make
	# the grab/drop dependency less messy.
	emit_signal("finished", "idle")