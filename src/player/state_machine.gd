# Tried to use `class_name` and extends, but inherited properties weren't showing
# up and it was throwing an error.
extends "res://src/state_machine.gd"

func _ready():
	# I'm curious if there's a better way without hardcoding in what
	# states there is?
	states_map = {
		"idle": $Idle,
		"walking": $Walking,
		"grab": $Grab,
		"drop": $Drop,
		"interact": $Interact
	}
	
	
func _change_state(state_name):
	if not _active:
		return
	# Handle states that can be sub states
	if state_name in ["drop", "grab"]:
		states_stack.push_front(states_map[state_name])
	._change_state(state_name)
	
	
func _unhandled_input(event):
	current_state.handle_input(event)

