extends "res://src/state.gd"

export(int) var MOVEMENT_SPEED = 10

export(NodePath) var kinematic_body_path
onready var kinematic_body: KinematicBody = get_node(kinematic_body_path)

export(NodePath) var animation_tree_path
onready var animation_tree: AnimationTree = get_node(animation_tree_path)
onready var animation_sm = animation_tree.get("parameters/playback")

var camera: Spatial

func enter():
	animation_sm.travel("walking")
	
	
# warning-ignore:unused_argument
func update(delta):
	var velocity := Vector3()
	var camera_basis := Basis()
	if camera != null:
		camera_basis = camera.transform.basis
	# Use the camera basis so that we are always moving relative to the camera.
	if (Input.is_action_pressed("move_back")):
		velocity += camera_basis.z
	elif (Input.is_action_pressed("move_forward")):
		velocity += -camera_basis.z
		
	if(Input.is_action_pressed("move_left")):
		velocity += -camera_basis.x
	elif(Input.is_action_pressed("move_right")):
		velocity += camera_basis.x
		
	# Reset the y velocity as the camera is always angled.
	velocity.y = 0
	
	if (velocity.length() == 0):
		emit_signal("finished", "idle")
		return
		
	if Input.is_action_just_pressed("grab_and_drop"):
		emit_signal("finished", "grab")
		return
		
	# Rotate towards the movement
	var angle = atan2(velocity.x, velocity.z)
	var rot := kinematic_body.get_rotation()
	rot.y = angle
	kinematic_body.set_rotation(rot)
	
	velocity = velocity.normalized() * MOVEMENT_SPEED
		
	# warning-ignore:return_value_discarded
	kinematic_body.move_and_slide(velocity, Vector3.UP, true)

