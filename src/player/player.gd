extends KinematicBody

export(NodePath) var camera_path
onready var camera: Spatial = get_node(camera_path)

func _ready():
	$StateMachine/Walking.camera = camera

# warning-ignore:unused_argument
func _physics_process(delta):
	# warning-ignore:return_value_discarded
	move_and_slide(Vector3(0, -Globals.GRAVITY, 0), Vector3.UP, true)
