extends Control

export(PackedScene) var starting_scene

func _on_Start_pressed():
	# TODO: Have a smarter scene resolution
	get_tree().change_scene_to(starting_scene)


func _on_Quit_pressed():
	get_tree().quit()
