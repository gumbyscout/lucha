extends Camera

export(NodePath) var targetNode
onready var target = get_node(targetNode)

onready var offset : Vector3 = get_global_transform().origin

# warning-ignore:unused_argument
func _physics_process(delta):
	var targetOrigin = target.get_global_transform().origin
	var pos = offset + targetOrigin
	look_at_from_position(pos, targetOrigin, Vector3.UP)
