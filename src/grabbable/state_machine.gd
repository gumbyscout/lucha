extends "res://src/state_machine.gd"

func _ready():
	states_map = {
		"idle": $Idle,
		"grabbed": $Grabbed,
		"held": $Held,
		"dropped": $Dropped,
	} 