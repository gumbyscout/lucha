extends "res://src/state.gd"

export(NodePath) var kinematic_body_path
onready var kinematic_body: KinematicBody = get_node(kinematic_body_path)

# warning-ignore:unused_argument
func update(delta):
	# warning-ignore:return_value_discarded
	kinematic_body.move_and_slide(Vector3(0, -Globals.GRAVITY, 0), Vector3.UP)
