extends KinematicBody

func _ready():
	add_to_group("grabbable")

func grab():
	# This kind feels gross
	$StateMachine._change_state("grabbed")
	
func drop():
	$StateMachine._change_state("dropped")