extends "res://src/grabbable/grabbable.gd" # TODO: another instance where class_name fell apart and had to use the path

func use(bodies: Array):
	for body in bodies:
		if body.has_method("ignite"):
			body.ignite()