tool
extends StaticBody

signal activated
signal deactivated

export var is_lit = false

func _ready():
	if is_lit:
		$AnimationPlayer.play("lit")
	else:
		$AnimationPlayer.play("unlit")

func ignite():
	if is_lit:
		return
	is_lit = true
	$AnimationPlayer.play("lit")
	emit_signal("activated")

func snuff_out():
	if !is_lit:
		return
	is_lit = false
	$AnimationPlayer.play("unlit")
	emit_signal("deactivated")