shader_type spatial;

uniform sampler2D texture_albedo : hint_albedo;
uniform float speed = 1;
uniform float tileScaleX = 1;
uniform float tileScaleY = 1;
uniform bool is_active = true;

void vertex() {
	// Scroll the texture at a constant speed up the y axis of the texture
	vec2 uv_offset = is_active ? vec2(0, TIME * speed) : vec2(0, 0);
	vec2 tileScale = vec2(tileScaleX, tileScaleY);
	UV = UV * tileScale.xy + uv_offset.xy;
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo_tex.rgb;
}
