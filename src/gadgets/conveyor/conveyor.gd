extends StaticBody

# With how materials and shaders work, changing the speed for one conveyor will change the visual 
# speed of every conveyor sharing the same material.
export(int) var speed := 5
export(bool) var is_active := true

onready var _shader_ref := $ConveyorTop.get_surface_material(0) as ShaderMaterial
var _has_duplicated_material := false

var _initial_speed := 0
var _bodies_entered := 0

func _ready():
	if !is_active:
		_on_deactivated()
	
	_shader_ref.set_shader_param("speed", speed)
	_shader_ref.set_shader_param("tileScaleY", self.scale.z)
	_initial_speed = speed;


func _physics_process(delta):
	# Skip processing if nothing has entered.
	if _bodies_entered < 1 or !is_active:
		return
		
	if _initial_speed != speed:
		_shader_ref.set_shader_param("speed", speed)
		_initial_speed = speed;
	
	var basis = transform.basis;
	var bodies: Array = $MoveArea.get_overlapping_bodies()
	for body in bodies:
		if body is KinematicBody:
			body.move_and_slide(-basis.z * speed)
			

func _on_MoveArea_body_entered(body):
	_bodies_entered += 1
	

func _on_MoveArea_body_exited(body):
	_bodies_entered -= 1


func _on_activated():
	is_active = true
	_shader_ref.set_shader_param("is_active", true)
	
	
func _on_deactivated():
	# Materials by default are shared between all instances, so if you change params on one, it changes them on everything.
	# I'm not sure how inefficient it'd be to have every conveyor have it's own material, but I think just giving it its
	# own if it has been toggled should be sufficient for now.
	if !_has_duplicated_material:
		var dup := _shader_ref.duplicate()
		$ConveyorTop.set_surface_material(0, dup)
		_shader_ref = dup
		_has_duplicated_material = true
		
	is_active = false
	_shader_ref.set_shader_param("is_active", false)
