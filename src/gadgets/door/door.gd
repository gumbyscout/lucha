extends StaticBody

func _ready():
	$AnimationPlayer.play("default")
	
func _on_activated():
	$AnimationPlayer.play("open")
	
func _on_deactivated():
	$AnimationPlayer.play("close")
