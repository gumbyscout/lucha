extends StaticBody

onready var button_mat := $Button.get_surface_material(0) as ShaderMaterial
var _num_bodies := 0

signal activated
signal deactivated

# warning-ignore:unused_argument
func _on_Area_body_entered(body):
	if _num_bodies == 0:
		button_mat.set_shader_param("is_active", true)
		emit_signal("activated")
	_num_bodies += 1


# warning-ignore:unused_argument
func _on_Area_body_exited(body):
	_num_bodies -= 1
	if _num_bodies <= 0:
		button_mat.set_shader_param("is_active", false)
		emit_signal("deactivated")
