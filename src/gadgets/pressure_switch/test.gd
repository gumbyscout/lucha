extends "res://addons/gut/test.gd"

var PressureSwitch := load("res://src/pressure_switch/pressure_switch.gd")	

var _obj	

var ACTIVATED := "activated"
var DEACTIVATED := "deactivated"

func before_each():
	_obj = PressureSwitch.new()
	# Stub out the shader
	_obj.button_mat = ShaderMaterial.new()


func test_has_required_signals():
	assert_has_signal(_obj, ACTIVATED)
	assert_has_signal(_obj, DEACTIVATED)
	

func test_entered_first_body():
	watch_signals(_obj)
	
	_obj._on_Area_body_entered(null)
	
	assert_signal_emitted(_obj, ACTIVATED)
	assert_eq(_obj._num_bodies, 1)
	
	
func test_entered_more_bodies():
	watch_signals(_obj)
	_obj._num_bodies = 1
	
	_obj._on_Area_body_entered(null)
	
	assert_signal_not_emitted(_obj, ACTIVATED)
	assert_eq(_obj._num_bodies, 2)
		
		
func test_exited_not_last_body():
	watch_signals(_obj)
	_obj._num_bodies = 2
	
	_obj._on_Area_body_exited(null)
	
	assert_signal_not_emitted(_obj, DEACTIVATED)
	assert_eq(_obj._num_bodies, 1)
	
		
func test_exited_last_body():
	watch_signals(_obj)
	_obj._num_bodies = 1
	
	_obj._on_Area_body_exited(null)
	
	assert_signal_emitted(_obj, DEACTIVATED)
	assert_eq(_obj._num_bodies, 0)