extends Spatial

onready var shader_mat := $MeshInstance.get_surface_material(0) as ShaderMaterial

func _on_activated():
	shader_mat.set_shader_param("is_active", true)
	
func _on_deactivated():
	shader_mat.set_shader_param("is_active", false)
